package br.com.campanha.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import br.com.campanha.models.Campanha;
import br.com.campanha.repository.CampanhaRepository;

public class CampanhaServiceTest {

	private CampanhaRepository repository;

	@Before
	public void init(){
		repository = mock(CampanhaRepository.class);
	}
	
	private Campanha novaCampanha() {
		return new Campanha("Teste", "Corinthians", LocalDate.now(), LocalDate.now().plusDays(1));
	}
	
	@Test
	public void novaCampanhaTest(){
		Campanha campanha = novaCampanha();
		Campanha campanha2 = novaCampanha();
		CampanhaService service = new CampanhaService(repository);
		
		when(repository.findByFimCampanha(campanha.getFimCampanha())).thenReturn(Arrays.asList(campanha2));
		when(repository.save(campanha2)).thenReturn(campanha2.atualizaDataVigente());
		
		service.nova(campanha);
		
		assertEquals(campanha.getFimCampanha(), LocalDate.now().plusDays(1));
		assertEquals(campanha2.getFimCampanha(), LocalDate.now().plusDays(2));
	}
	
	@Test
	public void atualizaDataVigenteTest(){
		LocalDate dataFinal = LocalDate.now().plusDays(10);
		CampanhaService service = new CampanhaService(repository);
		Campanha campanha = new Campanha("Teste", "Corinthians", LocalDate.now(), dataFinal);
		
		service.atualizaDataVigente(campanha);
		
		assertEquals(campanha.getFimCampanha(), dataFinal.plusDays(1));
	}
	
	@Test
	public void filtraCampanhaDuplicadaTest(){
		Campanha campanha = novaCampanha();
		CampanhaService service = new CampanhaService(repository);
		
		when(repository.findByFimCampanha(LocalDate.now().plusDays(1))).thenReturn(Arrays.asList());
		assertFalse(service.filtraCampanhaDuplicada(campanha));
		
		when(repository.findByFimCampanha(LocalDate.now().plusDays(1))).thenReturn(Arrays.asList(campanha));
		assertTrue(service.filtraCampanhaDuplicada(campanha));
	}
	
	@Test
	public void buscaCampanhasDuplicadasTest(){
		int count = 0;

		CampanhaService service = new CampanhaService(repository);
		Set<Campanha> duplicadas = new HashSet<>();

		when(repository.listaCampanhasAtivas()).thenReturn(Arrays.asList(novaCampanha(), novaCampanha(), novaCampanha()));
		
		when(repository.save(novaCampanha())).thenReturn(novaCampanha().atualizaDataVigente());
		
		duplicadas = service.filtraCampanhasAtivasDuplicadas();
		
		while(!duplicadas.isEmpty()){
			duplicadas.parallelStream().forEach(d -> service.atualizaDataVigente(d));
			duplicadas = service.filtraCampanhasAtivasDuplicadas();
			count += 1;
		};
		
		assertEquals(count, 3);
	}
	
	
}

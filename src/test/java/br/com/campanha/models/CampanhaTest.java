package br.com.campanha.models;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

public class CampanhaTest {

	@Test
	public void atualizaDataVigenteTest(){
		Campanha campanha = new Campanha("Teste 001", "Corinthians", LocalDate.now(), LocalDate.now().plusDays(1));
		assertEquals(campanha.atualizaDataVigente().getFimCampanha(), LocalDate.now().plusDays(2));
	}
}

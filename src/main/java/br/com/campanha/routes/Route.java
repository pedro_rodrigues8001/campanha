package br.com.campanha.routes;

import spark.servlet.SparkApplication;

public interface Route extends SparkApplication, Util{

	void filter();

}

package br.com.campanha.routes.impl;

import static br.com.campanha.convert.JsonTransformer.jsonTransformer;
import static br.com.campanha.convert.JsonUtil.*;
import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.before;
import static spark.Spark.halt;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import br.com.campanha.models.Campanha;
import br.com.campanha.routes.Route;
import br.com.campanha.service.CampanhaService;
import br.com.campanha.validation.BeanValidation;

@Component
public class RouteImpl implements Route {

	private CampanhaService campanhaService;

	@Override
	public void init() {

		config();

		exeptionHandler();
		
		filter();

		path("/campanha", () -> {

			post("/nova", (req, resp) -> {
				campanhaService = getServico(CampanhaService.class);
				Campanha campanha = asObject(req.body(), Campanha.class);
				campanha.ajusteTimeCoracao();
				return campanhaService.nova(campanha);
			}, jsonTransformer());

			get("/busca/:id", (req, resp) -> {
				campanhaService = getServico(CampanhaService.class);
				return campanhaService.busca(req.params(":id"));
			}, jsonTransformer());

			get("/lista", (req, resp) -> {
				campanhaService = getServico(CampanhaService.class);
				return campanhaService.listaAtivas();
			}, jsonTransformer());
			
			get("/lista/:time-coracao", (req, resp) -> {
				campanhaService = getServico(CampanhaService.class);
				return campanhaService.campanhasAtivasPeloTimeCoracao(req.params(":time-coracao").toLowerCase());
			}, jsonTransformer());


		});

	}

	@Override
	public void filter() {
		
		path("/campanha", () ->{
			
			before("/nova", (req, resp) -> {
				Campanha campanha = asObject(req.body(), Campanha.class);
				List<String> validation = BeanValidation.executeBeanValidation(campanha);
				validation.parallelStream().forEach(e -> halt(400, asJson(e)));
			});
			
			after("/nova", (req, resp) -> {
				new Thread( () -> {
					Set<Campanha> campanhasDuplicadas = campanhaService.filtraCampanhasAtivasDuplicadas();
					while(!campanhasDuplicadas.isEmpty()){
						campanhasDuplicadas.forEach(c -> campanhaService.atualizaDataVigente(c));
						campanhasDuplicadas = campanhaService.filtraCampanhasAtivasDuplicadas();
					};
				}).start();
			});
			
		});

	}

	

}

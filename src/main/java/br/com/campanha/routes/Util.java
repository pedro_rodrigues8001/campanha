package br.com.campanha.routes;

import static br.com.campanha.convert.JsonUtil.asJson;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.stop;

import java.util.Map;

import br.com.campanha.infra.AppContext;
import spark.Request;
import spark.Response;

public interface Util {

	default void config() {

		before((Request request, Response response) -> {
			response.header("Access-Control-Allow-Origin", "*");
			response.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
			response.header("Access-Control-Allow-Credentials", "true");
			response.header("Access-Control-Max-Age", "1600");
			response.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Access-Control-Request-Method, Authorization, X-Requested-With, Accept-Encoding");
			response.header("Content-Type", "application/json");
		});

		before((request, response) -> {
			if (request.raw().getMethod().equalsIgnoreCase("OPTIONS")) {
				return;
			}
		});

		options("/", (req, resp) -> "OPTIONS OK");

		get("/stop", "text/plain", (req, resp) -> {
			stop();
			return null;
		});

		after((request, response) -> {
			response.header("Content-Encoding", "gzip");
		});
	}

	default void exeptionHandler() {
		exception(Exception.class, (exception, request, response) -> {
			exception.printStackTrace();
			System.err.println(exception.getLocalizedMessage());
			response.status(500);
			response.body(asJson("Ops! Aconteceu um erro inesperado"));
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	default <T> T getServico(Class clazz) {
		Map<String, Object> filters = AppContext.getApplicationContext().getBeansOfType(clazz);
		return (T) filters.values().iterator().next();
	}
}

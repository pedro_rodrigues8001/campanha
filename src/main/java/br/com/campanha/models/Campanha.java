package br.com.campanha.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Campanha implements Serializable {

	private static final long serialVersionUID = -212610662480472510L;

	@Id
	@Expose(deserialize = false)
	private String id = UUID.randomUUID().toString();

	@Expose
	@NotEmpty(message = "Favor informar o nome da campanha")
	private String nome;

	@Expose
	@SerializedName("time_coracao")
	@NotEmpty(message = "Favor informar o time do coracão")
	private String timeCoracao;

	@Expose
	@Column
	@SerializedName("inicio_campanha")
	@NotNull(message = "Favor informar o inicio da campanha")
	private LocalDate inicioCampanha;

	@Expose
	@Column
	@SerializedName("fim_campanha")
	@NotNull(message = "Favor informar o fim da campanha")
	private LocalDate fimCampanha;
	
	public Campanha() {
	}
	
	public Campanha(String nome, String timeCoracao, LocalDate inicioCampanha, LocalDate fimCampanha) {
		this.nome = nome;
		this.timeCoracao = timeCoracao;
		this.inicioCampanha = inicioCampanha;
		this.fimCampanha = fimCampanha;
	}


	public String getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTimeCoracao() {
		return timeCoracao;
	}
	
	public void setTimeCoracao(String timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

	public LocalDate getInicioCampanha() {
		return inicioCampanha;
	}
	
	public void setInicioCampanha(LocalDate inicioCampanha) {
		this.inicioCampanha = inicioCampanha;
	}

	public LocalDate getFimCampanha() {
		return fimCampanha;
	}
	
	public void setFimCampanha(LocalDate fimCampanha) {
		this.fimCampanha = fimCampanha;
	}

	public Campanha atualizaDataVigente() {
		this.fimCampanha = this.fimCampanha.plusDays(1);
		return this;
	}
	
	public void ajusteTimeCoracao(){
		this.timeCoracao = timeCoracao.toLowerCase();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fimCampanha == null) ? 0 : fimCampanha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
        if (!(obj instanceof Campanha)) return false;
        Campanha campanha = (Campanha) obj;
        return campanha.fimCampanha.compareTo(fimCampanha) == 0;
	}
	

}

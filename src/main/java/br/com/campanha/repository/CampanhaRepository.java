package br.com.campanha.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import br.com.campanha.models.Campanha;

public interface CampanhaRepository extends Repository<Campanha, String>{

	Campanha save(final Campanha campanha);
	
	Campanha findByNome(final String nome);
	
	Campanha findById(final String id);
	
	List<Campanha> findByFimCampanha(final LocalDate date);
	
	@Query(nativeQuery = true, value = "SELECT * FROM campanha WHERE fim_campanha >= current_date AND time_coracao = ?1")
	List<Campanha> findByTimeCoracao(final String timeCoracao);
	
	List<Campanha> findAll();
	
	@Query(nativeQuery = true, value = "SELECT * FROM campanha WHERE fim_campanha >= current_date")
	List<Campanha> listaCampanhasAtivas();
}

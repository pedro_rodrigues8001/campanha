package br.com.campanha.interceptor;

import java.util.Arrays;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.campanha.convert.JsonUtil;
import br.com.campanha.models.Campanha;
import br.com.campanha.service.CampanhaService;

@Aspect
public class CampanhaInterceptor {

	private String url = "http://localhost:8181/socio-torcedor/atualiza/campanha/";

	@Autowired
	private CampanhaService service;

	@After("@annotation(br.com.campanha.annotations.RestInterceptor)")
	public void atualizaCampanhas() {
		new Thread( () -> {
			List<Campanha> campanhas = service.listaAtivas();
			campanhas.parallelStream().forEach(c -> {
				try {
					HttpClient httpClient = HttpClients.createDefault();
					HttpPut request = new HttpPut(url + c.getTimeCoracao());
					StringEntity params = new StringEntity(JsonUtil.asJson(Arrays.asList(c)));
					params.setContentType("application/json");
					request.setEntity(params);
					httpClient.execute(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}).start();
	}
}

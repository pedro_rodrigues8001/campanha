package br.com.campanha.service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.campanha.annotations.RestInterceptor;
import br.com.campanha.models.Campanha;
import br.com.campanha.repository.CampanhaRepository;

@Service
public class CampanhaService implements Serializable {

	private static final long serialVersionUID = -2853569683996667646L;

	@Autowired
	private CampanhaRepository repository;

	public CampanhaService() {
	}

	public CampanhaService(CampanhaRepository repository) {
		this.repository = repository;
	}

	@Transactional
	@RestInterceptor
	public Campanha nova(final Campanha campanha) {
		if(filtraCampanhaDuplicada(campanha))
			atualizaDataVigente(listaTodas());
		return repository.save(campanha);
	}

	@Transactional
	public List<Campanha> campanhasAtivasPeloTimeCoracao(final String timeCoracao){
		return repository.findByTimeCoracao(timeCoracao);
	}
	
	@Transactional
	public Campanha busca(final String id){
		return repository.findById(id);
	}
	
	@Transactional
	public List<Campanha> listaAtivas() {
		return repository.listaCampanhasAtivas();
	}

	@Transactional
	public Campanha save(final Campanha campanha) {
		return repository.save(campanha);
	}

	@Transactional
	public List<Campanha> buscaCampanhaPelaDataVigente(final LocalDate data) {
		return repository.findByFimCampanha(data);
	}

	@Transactional
	public List<Campanha> listaTodas(){
		return repository.findAll();
	}
	
	public Campanha atualizaDataVigente(final Campanha campanha){
		return save(campanha.atualizaDataVigente());
	}
	
	public void atualizaDataVigente(final List<Campanha> campanhas){
		campanhas.parallelStream().forEach(c -> save(c.atualizaDataVigente()));
	}
	
	public boolean filtraCampanhaDuplicada(final Campanha campanha) {
		List<Campanha> campanhas = buscaCampanhaPelaDataVigente(campanha.getFimCampanha());
		return campanhas != null && campanhas.size() >= 1;
	}
	
	public Set<Campanha> filtraCampanhasAtivasDuplicadas() {
		return campanhasDuplicadas(listaAtivas());
	}
	
	public Set<Campanha> filtraCampanhasDuplicadas() {
		return campanhasDuplicadas(listaTodas());
	}

	private Set<Campanha> campanhasDuplicadas(List<Campanha> campanhas) {
		Set<Campanha> all = new HashSet<>();
		return campanhas.parallelStream()
		        .filter(c -> !all.add(c))
		        .collect(Collectors.toSet());
	}
}

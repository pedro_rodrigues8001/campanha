package br.com.campanha.convert;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {

	private JsonUtil(){
    }

	public static <T> T asObject(String json, Class<T> clazz) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
									 .registerTypeAdapter(LocalDateTime.class, new DateTimeAdapter())
				 					 .registerTypeAdapter(LocalDate.class, new DateAdapter())
				 					 .create();
		
		return gson.fromJson(json, clazz);
	}

	public static <T> String asJson(T object) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
									 .registerTypeAdapter(LocalDateTime.class, new DateTimeAdapter())
									 .registerTypeAdapter(LocalDate.class, new DateAdapter())
									 .setPrettyPrinting().create();
		return gson.toJson(object);
	}
}

package br.com.campanha.convert;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import spark.ResponseTransformer;

public class JsonTransformer {

	public static String toJson(Object object) {
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new DateTimeAdapter())
									 .registerTypeAdapter(LocalDate.class, new DateAdapter())
									 .excludeFieldsWithoutExposeAnnotation()
									 .create();
		return gson.toJson(object);
	}

	public static ResponseTransformer jsonTransformer() {
		return JsonTransformer::toJson;
	}

}

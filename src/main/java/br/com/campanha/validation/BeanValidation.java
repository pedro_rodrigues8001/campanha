package br.com.campanha.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class BeanValidation {

private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	public static List<String> executeBeanValidation(final Object object) {
		try {
			Set<ConstraintViolation<Object>> violations = getViolations(factory.getValidator(), object);
			List<String> errors = new ArrayList<>();
			violations.stream().forEach(e -> errors.add(e.getMessage()));
			return errors;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	private static Set<ConstraintViolation<Object>> getViolations(final Validator validator, final Object object) {
		return validator.validate(object);
	}
}
